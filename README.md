
# NextJS-Locale (For New "App directory")

NextJS Tiny localization (~3kb) package for new **App directory** with **server components** + **client components** support






## Installation 

1. **Add nextjs-locale to your project with npm**
```bash
  npm install nextjs-locale
```


2. **Now all pages should be moved within a [lang] folder so that we can use this segment to provide content in different languages (e.g. /en, /en/about, etc.).** 
```md 
src
└── app
    └── [lang]
        ├── another-page
        |   └── page.js
        ├── layout.js
        └── page.js
```

3. **Add middleware to your src directory [./src/middleware.ts]**
```jsx
import createNextJsLocaleMiddleware from 'nextjs-locale/middleware'

export default createNextJsLocaleMiddleware({
    // A list of all locales that are supported
    locales: ['en', 'fa'],

    // If this locale is matched, pathnames will be removed from the URL (e.g., `/about`).
    defaultLocale: 'en'
})

export const config = {
    // Skip all paths that aren't pages that you'd like to internationalize
    matcher: ['/((?!api|_next|favicon.ico|assets).*)']
}

```
#### that's it! now you can use translations in your page and components.

## Usage
- **In Server pages and components**
```jsx 
import { useTranslate } from "nextjs-locale"

const ServerComponent = ({}) => {
    const { t, locale } = useTranslate()

    return (
        <div>
            {t(locales.test)}
        </div>
    )
}

const locales: any = {
    test: {
        en: 'Hello World!',
        fa: 'سلام دنیا‍!'
    }
}

export default ServerComponent
```
---

- **In Client page and components**
if you plans to use this library in your client component you need first add LocaleProvider (once) to your root Layout like this
```jsx
'use client'

import { LocaleProvider } from 'nextjs-locale/context'
import './globals.css'

export const generateMetadata = async ({ params: { lang } }: any) => {
    return { title: lang }
}

export default function RootLayout({
    children,
    params: { lang }
}: {
    children: React.ReactNode
    params: any
}) {
    return (
        <html lang={lang} dir={lang === 'fa' ? 'rtl' : 'ltr'}>
            <body>
                <LocaleProvider locale={lang}>{children}</LocaleProvider>
            </body>
        </html>
    )
}

```

#### then you can use translation in your all client components like server components:
```jsx
'use client'

import { useTranslate } from "nextjs-locale"

const ClientComponent = ({}) => {
    const { t, locale } = useTranslate()

    return (
        <div>
            {t(locales.test)}
        </div>
    )
}

const locales: any = {
    test: {
        en: 'Hello World!',
        fa: 'سلام دنیا‍!'
    }
}

export default ClientComponent
```

    

## Authors

- [@MehdiHS](https://www.gitlab.com/MehdiHS) -> [Mehdiw.ir](https://mehdiw.ir)

