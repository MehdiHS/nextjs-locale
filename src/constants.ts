// Reuse the legacy cookie name
// https://nextjs.org/docs/advanced-features/i18n-routing#leveraging-the-next_locale-cookie
export const COOKIE_LOCALE_NAME = 'NEXT_LOCALE'

// Should take precedence over the cookie
export const HEADER_LOCALE_NAME = 'X-NEXT-APP-LOCALE'
