export { default as useLocale } from './client/useClientLocale';
export { default as useTranslate } from './client/useClientTranslate';
