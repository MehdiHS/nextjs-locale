import 'client-only';
import LocaleContext from './LocaleContext';
import React from 'react';

export default function LocaleProvider({
  children,
  locale,
}: {
  children: React.ReactNode;
  locale: string;
}) {
  return (
    <LocaleContext.Provider
      value={{
        locale,
      }}
    >
      {children}
    </LocaleContext.Provider>
  );
}
