import 'client-only';
import { createContext } from 'react';

const LocaleContext = createContext<{ locale: string }>({
  locale: '',
});

export default LocaleContext;
