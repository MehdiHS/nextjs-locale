'use client';

import { useContext } from 'react';
import { default as LocaleContext } from '../context/LocaleContext';

export default function useClientLocale() {
  const context = useContext(LocaleContext);

  return context?.locale || '';
}
