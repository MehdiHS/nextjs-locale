'use client';

import useClientLocale from './useClientLocale';

const useClinetTranslate = () => {
  const locale = useClientLocale();

  const t = (textObject: any) => textObject[locale] || 'LANG_NOT_FOUND';

  return { t, locale };
};

export default useClinetTranslate;
