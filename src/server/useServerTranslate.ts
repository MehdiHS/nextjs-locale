import 'server-only';
import useServerLocale from './useServerLocale';

const useServerTranslate = () => {
  const locale = useServerLocale();

  const t = (textObject: any) => textObject[locale] || 'LANG_NOT_FOUND';

  return { t, locale };
};

export default useServerTranslate;
