import useLocale from './useServerLocale';
import useTranslate from './useServerTranslate';

export { useLocale, useTranslate };
