import { NextRequest, NextResponse } from 'next/server';
import { COOKIE_LOCALE_NAME, HEADER_LOCALE_NAME } from '../constants';
import resolveLocale from './resolveLocale';

const ROOT_URL = '/';

const createLocaleMiddleware = (config: {
  defaultLocale: string;
  locales: Array<string>;
}) => {
  return function middleware(request: NextRequest) {
    let response;
    const pathname = request.nextUrl.pathname;

    const { locale } = resolveLocale(
      config,
      request.cookies,
      request.nextUrl.pathname
    );

    const isRoot = request.nextUrl.pathname === ROOT_URL;

    const hasOutdatedCookie =
      request.cookies.get(COOKIE_LOCALE_NAME)?.value !== locale;

    const hasMatchedDefaultLocale = locale === config.defaultLocale;

    function getResponseInit() {
      let responseInit;

      if (hasOutdatedCookie) {
        // Only apply a header if absolutely necessary
        // as this causes full page reloads
        request.headers.set(HEADER_LOCALE_NAME, locale);
        responseInit = {
          request: {
            headers: request.headers,
          },
        };
      }

      return responseInit;
    }

    function rewrite(url: string) {
      return NextResponse.rewrite(new URL(url, request.url), getResponseInit());
    }

    function next() {
      return NextResponse.next(getResponseInit());
    }

    function redirect(url: string) {
      return NextResponse.redirect(new URL(url, request.url));
    }

    if (isRoot) {
      // On "/" URL
      let pathWithSearch = `/${locale}`;
      if (request.nextUrl.search) {
        pathWithSearch += request.nextUrl.search;
      }

      if (hasMatchedDefaultLocale) {
        response = rewrite(pathWithSearch);
      } else {
        response = redirect(pathWithSearch);
      }
    } else {
      //On other url with lang url

      // Check if there is any supported locale in the pathname
      const pathLocale = config.locales.find((cur) => {
        const string = `^\/${cur}$|^\/${cur}\/(.*)$`;
        const regexp = new RegExp(string);

        return regexp.test(pathname);
      });

      const isRootWithLocale =
        request.nextUrl.pathname === ROOT_URL + pathLocale;

      const hasLocalePrefix = pathLocale != null;

      let pathWithSearch = request.nextUrl.pathname;

      if (request.nextUrl.search) {
        pathWithSearch += request.nextUrl.search;
      }

      if (hasLocalePrefix) {
        if (pathLocale === locale) {
          if (pathLocale === config.defaultLocale) {
            const basePath = pathWithSearch.replace(
              `/${pathLocale}`,
              isRootWithLocale ? '/' : ''
            );
            response = redirect(basePath);
          } else {
            response = next();
          }
        } else {
          const basePath = pathWithSearch.replace(pathLocale, '');

          response = redirect(`/${locale}${basePath}`);
        }
      } else {
        if (hasMatchedDefaultLocale) {
          response = rewrite(`/${locale}${pathWithSearch}`);
        } else {
          response = redirect(`/${locale}${pathWithSearch}`);
        }
      }
    }

    if (hasOutdatedCookie) {
      response.cookies.set(COOKIE_LOCALE_NAME, locale);
    }

    return response;
  };
};

export default createLocaleMiddleware;
