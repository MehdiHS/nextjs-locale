import { RequestCookies } from 'next/dist/server/web/spec-extension/cookies';
import { COOKIE_LOCALE_NAME } from '../constants';

export default function resolveLocale(
  config: {
    defaultLocale: string;
    locales: Array<string>;
  },
  requestCookies: RequestCookies,
  pathname: string
) {
  let locale;

  // Prio 1: Use route prefix
  if (pathname) {
    const segments = pathname.split('/');
    if (segments.length > 1) {
      const segment = segments[1];
      if (config.locales.includes(segment)) {
        locale = segment;
      }
    }
  }

  // Prio 2: Use existing cookie
  if (!locale && requestCookies) {
    if (requestCookies.has(COOKIE_LOCALE_NAME)) {
      const value = requestCookies.get(COOKIE_LOCALE_NAME)?.value;
      if (value && config.locales.includes(value)) {
        locale = value;
      }
    }
  }

  // Prio 3: Use default locale
  if (!locale) {
    locale = config.defaultLocale;
  }

  return { locale };
}
